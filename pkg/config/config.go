package config

import (
	"os"
	"strconv"
)

const (
	portKey = "CINESHOP_ANON_USERS_PORT"

	jwtSecret = "CINESHOP_JWT_SECRET"
)

type Config struct {
	Port      int
	JWTSecret string
}

var config Config

func init() {
	portConfig()
	jwtSecretConfig()
}

func GetConfig() Config {
	return config
}

func portConfig() {
	config.Port = getInt(portKey, 8091)
}

func jwtSecretConfig() {
	config.JWTSecret = getString(jwtSecret, "ThisIsTheSuperSecretKey")
}

func getString(key string, defaultValue string) string {
	value := os.Getenv(key)
	if value == "" {
		return defaultValue
	}
	return value
}

func getInt(key string, defaultValue int) int {
	if value, ok := strconv.Atoi(os.Getenv(key)); ok == nil {
		return value
	}
	return defaultValue
}
