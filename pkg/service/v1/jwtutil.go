package impl

import (
	"time"

	"gitlab.com/cineshop-projects/cineshop-service-anonymous-users/pkg/config"

	"github.com/dgrijalva/jwt-go"
	"gitlab.com/cineshop-projects/cineshop-data-access/pkg/dao/model"
)

func GenerateToken(user model.User) (model.Token, error) {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"id":    user.Id,
		"email": user.Email,
		"iat":   time.Now(),
	})

	stringToken, err := token.SignedString([]byte(config.GetConfig().JWTSecret))
	return model.Token(stringToken), err
}
