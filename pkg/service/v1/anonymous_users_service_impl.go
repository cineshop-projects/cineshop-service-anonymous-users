package impl

import (
	"context"

	"gitlab.com/cineshop-projects/cineshop-data-access/pkg/dao"
	"gitlab.com/cineshop-projects/cineshop-data-access/pkg/dao/model"
	"gitlab.com/cineshop-projects/cineshop-service-anonymous-users/pkg/api/v1/domain"
	"gitlab.com/cineshop-projects/cineshop-service-anonymous-users/pkg/api/v1/service"
	"golang.org/x/crypto/bcrypt"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type AnonymousUsersService struct{}

func (s *AnonymousUsersService) Signin(_ context.Context, signInRequest *service.SignInRequest) (*domain.SignInResponse, error) {
	user, err := dao.GetUserDao().GetByEmail(signInRequest.Email)
	if err != nil {
		code := codes.Internal
		if err == model.EntityNotFound {
			code = codes.NotFound
		}
		return nil, status.Error(code, "Failed to retrieve User")
	}

	if err := bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(signInRequest.Password)); err != nil {
		return nil, status.Error(codes.PermissionDenied, "Failed to login")
	}

	token, err := s.generateToken(user)
	if err != nil {
		code := codes.Internal
		if err == model.EntityConflict {
			code = codes.AlreadyExists
		}
		return nil, status.Error(code, "Failed to open session")
	}

	return &domain.SignInResponse{
		FirstName:   user.FirstName,
		LastName:    user.LastName,
		Email:       user.Email,
		AccessToken: string(token),
	}, nil
}

func (s *AnonymousUsersService) Signup(_ context.Context, signUpRequest *service.SignUpRequest) (*domain.SignInResponse, error) {
	password, err := bcrypt.GenerateFromPassword([]byte(signUpRequest.Password), bcrypt.DefaultCost)
	if err != nil {
		return nil, status.Error(codes.Internal, "Failed to register")
	}

	user := model.User{
		FirstName: signUpRequest.FirstName,
		LastName:  signUpRequest.LastName,
		Email:     signUpRequest.Email,
		Password:  string(password),
	}
	user, err = dao.GetUserDao().Insert(user)
	if err != nil {
		code := codes.Internal
		if err == model.EntityConflict {
			code = codes.AlreadyExists
		}
		return nil, status.Error(code, "Failed to register")
	}

	token, err := s.generateToken(user)
	if err != nil {
		code := codes.Internal
		if err == model.EntityConflict {
			code = codes.AlreadyExists
		}
		return nil, status.Error(code, "Failed to open session")
	}

	return &domain.SignInResponse{
		FirstName:   user.FirstName,
		LastName:    user.LastName,
		Email:       user.Email,
		AccessToken: string(token),
	}, nil
}

func (s *AnonymousUsersService) generateToken(user model.User) (model.Token, error) {
	stringToken, err := GenerateToken(user)
	if err == nil {
		err = dao.GetTokenDao().Insert(stringToken)
	}

	return stringToken, err
}
