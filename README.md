# Go API Server for Cineshop Anonymous Users service

Cineshop gRCP service for sign up and sign in.

## Running the server
To run the server, follow these simple steps:

```shell script
go run pkg/cmd/main.go
```

Some environment variables could be specified to configure the access to data sources:

| Variable                       | Purpose                     | Default value           |
|--------------------------------|-----------------------------|-------------------------|
| `CINESHOP_ANON_USERS_PORT`     | Server port                 | 8091                    |
| `CINESHOP_JWT_SECRET `         | JWT signing key             | ThisIsTheSuperSecretKey |
| `CINESHOP_MOCK_REDIS`          | Mock Redis data source      | true                    |
| `CINESHOP_MOCK_POSTGRESQL`     | Mock PostgreSQL data source | true                    |
| `CINESHOP_MOCK_MONGODB`        | Mock MongoDB data source    | true                    |
| `CINESHOP_REDIS_HOST`          | Redis host                  | localhost               |
| `CINESHOP_REDIS_PORT`          | Redis port                  | 6379                    |
| `CINESHOP_REDIS_PASSWORD`      | Redis password              |                         |
| `CINESHOP_POSTGRESQL_HOST`     | PostgreSQL host             | localhost               |
| `CINESHOP_POSTGRESQL_PORT`     | PostgreSQL port             | 5432                    |
| `CINESHOP_POSTGRESQL_USER`     | PostgreSQL user             | postgres                |
| `CINESHOP_POSTGRESQL_PASSWORD` | PostgreSQL password         | password                |
| `CINESHOP_POSTGRESQL_DBNAME`   | PostgreSQL database name    | postgres                |
| `CINESHOP_MONGODB_HOST`        | MongoDB host                | localhost               |
| `CINESHOP_MONGODB_PORT`        | MongoDB port                | 27017                   |
| `CINESHOP_MONGODB_PASSWORD`    | MongoDB password            |                         |

All the values assigned to `CINESHOP_MOCK_REDIS`, `CINESHOP_MOCK_POSTGRESQL` and `CINESHOP_MOCK_MONGODB`, except the literal `false` regardless of the case, are translated to `true`.

To run the server in a docker container, first build the image:
```shell script
docker build --network=host -t cineshop-anon-users .
```

Then run:
```shell script
docker run --rm -it -p 8091:8091 --name cineshop-anon-users cineshop-anon-users
```


