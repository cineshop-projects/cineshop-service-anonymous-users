version ?= 0.9.0

all: publish-image

build-image:
	 docker build -t cineshop-service-anonymous-users .

publish-image: build-image
	docker tag cineshop-service-anonymous-users atefn/cineshop-service-anonymous-users:$(version)
	docker push atefn/cineshop-service-anonymous-users:$(version)

install-tools:
	go get -u google.golang.org/grpc
	go install google.golang.org/grpc
	go get -u google.golang.org/protobuf/cmd/protoc-gen-go
	go install google.golang.org/protobuf/cmd/protoc-gen-go
	go get -u google.golang.org/grpc/cmd/protoc-gen-go-grpc
	go install google.golang.org/grpc/cmd/protoc-gen-go-grpc
	go get -u golang.org/x/tools/cmd/goimports
	go install golang.org/x/tools/cmd/goimports

proto-gen: api/proto/v1
	mkdir -p ./pkg/api/v1
	protoc -I ./api/proto/v1 --go_out=./pkg/api/v1 --go_opt=paths=source_relative ./api/proto/v1/domain/*
	protoc -I ./api/proto/v1 --go_out=./pkg/api/v1 --go_opt=paths=source_relative --go-grpc_out=./pkg/api/v1 --go-grpc_opt=paths=source_relative ./api/proto/v1/service/*
	goimports -w ./pkg/api/v1

.PHONY: install-tools proto-gen build-image publish-image all