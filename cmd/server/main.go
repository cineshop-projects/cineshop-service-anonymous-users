package main

import (
	"context"
	"fmt"
	"log"
	"net"
	"os"
	"os/signal"
	"syscall"
	"time"

	"gitlab.com/cineshop-projects/cineshop-service-anonymous-users/pkg/api/v1/service"
	"gitlab.com/cineshop-projects/cineshop-service-anonymous-users/pkg/config"
	impl "gitlab.com/cineshop-projects/cineshop-service-anonymous-users/pkg/service/v1"
	"google.golang.org/grpc"
)

func main() {
	port := config.GetConfig().Port

	listener, err := net.Listen("tcp", fmt.Sprintf(":%v", port))
	if err != nil {
		log.Fatalf("Cineshop AnonymousUsers server failed to listen: %v", err)
	}

	server := grpc.NewServer()
	service.RegisterAnonymousUsersServiceService(server, service.NewAnonymousUsersServiceService(&impl.AnonymousUsersService{}))

	go func() {
		log.Printf("Cineshop AnonymousUsers server starting on port %v...", port)
		_ = server.Serve(listener)
	}()

	interruptChan := make(chan os.Signal, 1)
	signal.Notify(interruptChan, os.Interrupt, syscall.SIGINT, syscall.SIGTERM)
	<-interruptChan

	log.Println("Cineshop AnonymousUsers server shutting down...")

	_, cancel := context.WithTimeout(context.Background(), time.Second*30)
	defer cancel()
	server.GracefulStop()

	os.Exit(0)
}
